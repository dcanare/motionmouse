import sys
from PySide import QtCore, QtGui

from . import DwellOptions

class DwellSettings(QtGui.QDialog):
	def __init__(self):
		super().__init__()

		self.ui = DwellOptions.Ui_DwellOptionsDialog()
		self.ui.setupUi(self)
		self.handler = None
		self.setWindowFlags(self.windowFlags() ^ QtCore.Qt.WindowContextHelpButtonHint)
		
	def accept(self):
		if self.handler:
			if len(self.ui.indicatorList.selectedItems()) != 1:
				QtGui.QMessageBox.information(None, 'Dwell options', 'Please select an indicator')
				return
			
			self.handler.setOptions(
				self.ui.periodBox.value(),
				self.ui.rangeBox.value(),
				self.ui.indicatorList.selectedItems()[0].text()
			)
		self.hide()
		
	def reject(self):
		self.hide()

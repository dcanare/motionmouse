import sys
from PySide import QtCore, QtGui

from . import resources_rc

from .dwellsettings import *
from .indicator import *

app = QtGui.QApplication(sys.argv)
app.setQuitOnLastWindowClosed(False)
indicator = Indicator()
dwellSettingsWindow = DwellSettings()

icon = QtGui.QSystemTrayIcon(QtGui.QIcon(':/graphics/icon.png'))

def iconActivated(reason):
	if reason != icon.ActivationReason.Context:
		pos = icon.geometry().center()
		icon.contextMenu().move(pos.x(), pos.y())
		icon.contextMenu().show()

def setMenuHandler(handler):
	menu = QtGui.QMenu()
	
	action = QtGui.QAction('Do mouse control', menu, checkable=True)
	action.setChecked(True)
	action.toggled.connect(handler.toggleMouseControl)
	menu.addAction(action)
	
	menu.addAction('Dwell options...').triggered.connect(handler.showDwellOptions)
	menu.addAction('Quit').triggered.connect(handler.exit)
	icon.setContextMenu(menu)
	icon.activated.connect(iconActivated)
	icon.activated.connect(handler.iconActivated)
	
	dwellSettingsWindow.handler = handler
	
def showDwellOptions(period, range):
	dwellSettingsWindow.ui.periodBox.setValue(period)
	dwellSettingsWindow.ui.rangeBox.setValue(range)
	dwellSettingsWindow.show()
	
def setStatus(status):
	icon.contextMenu().actions()[0].setText("Status : %s" % status)
	
def showMessage(msg):
	msgBox = QtGui.QMessageBox()
	msgBox.setText(msg)
	msgBox.setWindowTitle('MotionMouse')
	msgBox.setStandardButtons(QtGui.QMessageBox.Ok)
	return msgBox.exec()

def start():
	icon.show()
	sys.exit(app.exec_())

def exit():
	app.quit()


import sys
from PySide import QtCore, QtGui

from . import resources_rc
from . import DwellOptions

class Indicator(QtGui.QMainWindow):
	moveSignal = QtCore.Signal(int, int)
	showSignal = QtCore.Signal()
	hideSignal = QtCore.Signal()

	def __init__(self):
		super().__init__(
			parent=None,
			flags=QtCore.Qt.FramelessWindowHint|QtCore.Qt.WindowStaysOnTopHint|QtCore.Qt.ToolTip|QtCore.Qt.Dialog
		)
		self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
		
		self.moveSignal.connect(self._move)
		self.showSignal.connect(self._show)
		self.hideSignal.connect(self._hide)

		self.label = QtGui.QLabel('')
		self.setCentralWidget(self.label)
		self.setImage('target')
		
	def setImage(self, resourceName):
		pixmap = QtGui.QPixmap(":/graphics/%s.png" % resourceName )
		self.label.setPixmap(pixmap)
		self.setFixedSize(pixmap.size())
		
	def move(self, x, y):
		x = int(x - self.width()/2 + 1)
		y = int(y - self.height()/2 + 1)
		self.moveSignal.emit(x, y)

	def _move(self, x, y):
		self._show()
		super().move(x, y)
		
	def show(self):
		self.showSignal.emit()

	def _show(self):
		super().show()

	def hide(self):
		self.hideSignal.emit()

	def _hide(self):
		super().hide()


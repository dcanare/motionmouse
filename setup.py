from distutils.core import setup
import os, sys, time, shutil
import py2exe

shutil.rmtree('dist/', True)

time.sleep(2)
os.mkdir('dist')

sys.argv.append('py2exe')

setup(
    name = 'MouseMotion',
    version = time.strftime('%Y-%m-%d'),
    author = 'Dominic Canare',
    author_email = 'dom@greenlighgo.org',
    windows = [{'script':'MotionMouse.py', 'icon_resources': [(1, 'gui/graphics/icon.ico')]}],
#    console = ['MotionMouse.py'],
    description = 'LeapMotion Mouse Control',
    zipfile = 'modules.lib',
    options = {
        'py2exe': {
            'excludes': [
                'email', 'http', 'pydoc_data', 'pywin', 'urllib', 'xml',
                'doctest', 'pdb', 'inspect', 'distutils',
                'pyreadline', 'optparse', 'calendar'
            ],
            'optimize': 2,
            'compressed': True,
        },
    }
)

shutil.copyfile('LeapPython.dll', 'dist/LeapPython.dll')

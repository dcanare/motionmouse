# -*- coding: utf-8 -*-
'''
    A Leap Motion-controlled mouse cursor
    
    Dominic Canare <dom@greenlightgo.org>
'''

import sys, time, math
from win32api import GetSystemMetrics

import Leap
from pymouse import PyMouse
import keyboard_hook

from tcp_broadcaster import TCPBroadcaster

from selection_detector import DwellSelect, Point

class Listener(Leap.Listener):
    def __init__(self):
        super(Listener, self).__init__()
        self.screenSize = [ GetSystemMetrics(0), GetSystemMetrics(1) ]
        self.mouse = PyMouse()

        self.dwellSelect = DwellSelect(.05, 3)
        self.clickCount = 0
        
        self.lastBroadcastCoords = [-1, -1]
        self.broadcaster = TCPBroadcaster()
        self.broadcaster.start()

    def on_init(self, controller):
        print "Initialized"
        controller.enable_gesture(Leap.Gesture.TYPE_CIRCLE)
        controller.config.set("Gesture.Circle.MinRadius", 2.0)

    def on_connect(self, controller):
        print "Connected"

    def on_disconnect(self, controller):
        print "Disconnected"

    def on_frame(self, controller):
        # Get the most recent frame and report some basic information
        frame = controller.frame()
        currentTime = time.time()

        if not frame.hands.is_empty:
            interactionBox = frame.interaction_box
            def toScreenCoordinates(pos):
                pos = Leap.Vector(pos[0], pos[1] - 60, pos[2])
                pos = interactionBox.normalize_point(pos)
                return [
                    int(pos.x * self.screenSize[0]),
                    int(self.screenSize[1] - pos.y * self.screenSize[1]),
                    pos.z
                ]

            pointable = frame.pointables.frontmost
            if not pointable.is_valid:
                return
                
            coords = toScreenCoordinates(pointable.stabilized_tip_position)

            pointableAngle = pointable.direction * math.pi / 2
            pointableAngle = [pointableAngle[0], pointableAngle[1] + .5, pointableAngle[2]]
            adjust = 0.25
            coords[0] += int(math.tan(pointableAngle[0]) * coords[2] * self.screenSize[0] * adjust)
            coords[1] -= int(math.tan(pointableAngle[1]) * coords[2] * self.screenSize[1] * adjust)

            self.dwellSelect.addPoint(Point(coords[0], coords[1], 0, currentTime, frame))
            self.mouse.move(coords[0], coords[1])
            
            if self.dwellSelect.selection != None:
                p = self.dwellSelect.selection
                self.broadcaster.send("d\t%d\t%d" % (p.x, p.y))
            else:
                self.broadcaster.send("cd")
            
            for gesture in frame.gestures():
                if gesture.state == Leap.Gesture.STATE_UPDATE:
                    gesture = Leap.CircleGesture(gesture)   
                    if self.clickCount < int(gesture.progress + .15):
                        self.clickCount += 1
                        selectionPoint = self.dwellSelect.selection
                        if selectionPoint != None:
                            print "Clicking on", selectionPoint
                            self.broadcaster.send("c")
                            self.mouse.click(selectionPoint.x, selectionPoint.y)
                if gesture.state == Leap.Gesture.STATE_STOP:
                    selectionPoint = self.dwellSelect.clearSelection()
                    self.clickCount = 0

killKeyDetected = False
def keyListener(event):
    global killKeyDetected

    if event.Key == "F8" and event.Alt > 0:
        killKeyDetected = True
        keyboard_hook.stop()

keyboard_hook.handlers.append(keyListener)
keyboard_hook.listen()


listener = Listener()
controller = Leap.Controller()
controller.set_policy_flags(Leap.Controller.POLICY_BACKGROUND_FRAMES)

controller.add_listener(listener)

while not killKeyDetected:
    time.sleep(0)

# Remove the sample listener when done
controller.remove_listener(listener)

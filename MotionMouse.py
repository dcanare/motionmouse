import sys, time, math, traceback
sys.path.insert(0, '../')

from win32api import GetSystemMetrics

import Leap
from Leap import CircleGesture, KeyTapGesture, ScreenTapGesture, SwipeGesture

from selection_detector import DwellSelect, Point
import gui, mouse

from PySide import QtCore, QtGui


class LeapListener(Leap.Listener):
    def __init__(self):
        super().__init__()
        self.screenSize = [ GetSystemMetrics(0), GetSystemMetrics(1) ]

        self.dwellSelect = DwellSelect(.05, 3)
        self.clickCount = 0
        self.enabled = True
        self.connected = False

    def on_connect(self, controller):
        self.connected = True
        
    def on_disconnect(self, controller):
        self.connected = False
        
    def setEnabled(self, enabled):
        self.enabled = enabled
    
    def getStatus(self):
        return "%s | %s" % (
            'Enabled' if self.enabled else 'Disabled',
            'Connected' if self.connected else 'Disconnected'
        )

    def on_frame(self, controller):
        if not self.enabled:
            gui.indicator.hide()
            return
        
        frame = controller.frame()
        currentTime = time.time()

        if frame.hands.is_empty:
            gui.indicator.hide()
        else:
            pointable = frame.pointables.frontmost
            if not pointable.is_valid:
                return
                
            interactionBox = frame.interaction_box
            stabilizedPosition = pointable.stabilized_tip_position
            normalizedPosition = interactionBox.normalize_point(stabilizedPosition)
            
            coords = [
                normalizedPosition.x * self.screenSize[0],
                self.screenSize[1] - normalizedPosition.y * self.screenSize[1]
            ]

            self.dwellSelect.addPoint(Point(coords[0], coords[1], 0, currentTime, frame))
            mouse.move(coords[0], coords[1])
            
            if self.dwellSelect.selection != None:
                p = self.dwellSelect.selection
                gui.indicator.move(p.x, p.y)
            else:
                gui.indicator.hide()
            
            for gesture in frame.gestures():
                if gesture.state == Leap.Gesture.STATE_UPDATE:
                    gesture = Leap.CircleGesture(gesture)   
                    if self.clickCount < int(gesture.progress + .15):
                        self.clickCount += 1
                        selectionPoint = self.dwellSelect.selection
                        if selectionPoint != None:
                            gui.indicator.hide()
                            mouse.click(selectionPoint.x, selectionPoint.y)
                if gesture.state == Leap.Gesture.STATE_STOP:
                    selectionPoint = self.dwellSelect.clearSelection()
                    self.clickCount = 0

def main():
    controller = Leap.Controller()
    controller.set_policy_flags(Leap.Controller.POLICY_BACKGROUND_FRAMES)
    controller.enable_gesture(Leap.Gesture.TYPE_CIRCLE);
    listener = LeapListener()

    class MenuHandler(QtCore.QObject):
        def setOptions(self, period, range, indicator):
            listener.dwellSelect.minimumDelay = period
            listener.dwellSelect.range = range
            gui.indicator.setImage(indicator)
            
        def iconActivated(self, reason):
            gui.setStatus(listener.getStatus())

        def toggleMouseControl(self, enabled):
            listener.setEnabled(enabled)
            if listener.enabled:
                controller.add_listener(listener)
            else:
                controller.remove_listener(listener)
            
        def showDwellOptions(self):
            gui.showDwellOptions(
                listener.dwellSelect.minimumDelay,
                listener.dwellSelect.range
            )

        def exit(self):
            self.toggleMouseControl(False)
            gui.exit()
            
    handler = MenuHandler()
    gui.setMenuHandler(handler)
    controller.add_listener(listener)
    
    gui.start()

if __name__ == "__main__":
    me = None
    try:
        from tendo import singleton
        me = singleton.SingleInstance()
    except:
        print(traceback.format_exc())
        gui.showMessage('Another instance is already running')
        gui.exit()
        
    if me != None:
        main()


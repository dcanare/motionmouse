from gui.DwellOptions import *

import model, sound, speech

from PySide import QtCore

class DwellSettings(QtGui.QMainWindow):
	def __init__(self):
		super().__init__()

		self.ui = Ui_DwellOptionsDialog()
		self.ui.setupUi(self)
